var http = require("http");
var templater = require("./templater");
var USE_TEMPLATE_NAME = "template.tn";
templater.setTemplateFolder( "./" );

startServer();

function templateIsReady(callBack, errorBack)
{
	var replacer = new Array();
	replacer["MYNAME"] = "Stevica";
	replacer["MYSURNAME"] = "Kuharski";
	templater.sectionToShow( USE_TEMPLATE_NAME, "start", replacer, function(data){
		callBack(data);
	}, function(){
		errorBack( "Error in array" );
	});
}

function startServer()
{
	http.createServer(function(request, response) {
		response.writeHead(200, {"Content-Type": "text/html"});
		templater.loadFile(USE_TEMPLATE_NAME, function() {
			templater.prepareSections( USE_TEMPLATE_NAME, function(){
			  	templateIsReady(function(data){
			  		response.write(data);
			  		response.end();
			  	}, function(error){
			  		response.write(error);
			  		response.end();
			  	});				
			});
		}, function(error){
	  		response.write("Error: " + error);
	  		response.end();
		});

	}).listen(8888);
}