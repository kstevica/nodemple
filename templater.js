var _testProperty;
var _files = new Array();
var _fileData = new Array();
var _sections = new Array();
var _templateFolder;

var fs = require("fs");
var funcs = require("./npjsfuncs");

exports.loadFile = function(fileName, callBack, errorBack)
{
	_files[fileName] = fileName;
	_fileData[fileName] = new Array();

	fs.readFile(_templateFolder + fileName, "utf8", function(error, data){
		if (error) {
			errorBack(error);
		} else {
			_fileData[fileName] = data.split("\n");
			_sections[fileName] = new Array();
			callBack();
		}
	});
}

function createSectionsFromFile( fileName )
{
	for (var i=0; i<_fileData[fileName].length; i++) {
		var line = _fileData[fileName][i];
		if (line.substring(0, 10)=="<!--BEGIN:") {
			var sectionName = line.substring( 10, line.length-3 );
			_sections[fileName][sectionName] = new Array();
			extractSectionFromFile( fileName, sectionName, i );
		}
	}
}

function extractSectionFromFile( fileName, sectionName, fromLine )
{		
	for (var i=fromLine+1; i<_fileData[fileName].length; i++) {
		var line = _fileData[fileName][i];
		if (line.substring(0, 10)=="<!--BEGIN:") {
			var sectionName = line.substring( 10 );
			extractSectionFromFile( fileName, sectionName, i );
		} else if (line=="<!--END:" + sectionName + "-->") {
			break;
		} else {
			_sections[fileName][sectionName].push( line );
		}
	}
}

exports.prepareSections = function(fileName, callBack) 
{
	createSectionsFromFile( fileName );
	callBack();
}

exports.setTemplateFolder = function( folder ) 
{
	_templateFolder = folder;
}

exports.getTemplateFolder = function()
{
	return _templateFolder;
}

exports.sectionToShow = function( fileName, sectionName, properties, callBack, errorBack )
{
	if (properties instanceof Array) {
		var returnString = "";
		returnString += sectionToText( fileName, sectionName, properties );
		callBack( returnString );
	} else {
		errorBack();
	}
}

function sectionToText( fileName, sectionName, properties )
{
	var returnString = "";
	for (var i=0; i<_sections[fileName][sectionName].length;i++) {
		var line = _sections[fileName][sectionName][i];		
		if (line.substring( 0, 12 ) == "<!--INCLUDE:") {
			var newSection = line.split(":");
			if (newSection.length==2) {
				var newSectionName = newSection[1].substring(0, newSection[1].length-3);
				var newLine = sectionToText( fileName, newSectionName, new Array() );
				returnString += newLine;
			}	
		} else if (line.substring(0, 9)=="<!--EVAL:") {
			var functionName = "funcs." + line.substring( 9, line.length-3 );
			console.log( fileName + "->" + sectionName);
			eval(functionName);			
		} else {
			returnString += line;
		}
	}
	for (var key in properties) {
		returnString = returnString.replaceAll("{" + key + "}", properties[key]);
	}
	return returnString;
}

String.prototype.replaceAll = function (find, replace) {
    var str = this;
    return str.replace(new RegExp(find, 'g'), replace);
};